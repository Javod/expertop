
class Experto
{
  String id;
  String nombre;
  String apellidos;
  String especialidad;
  String etiqueta;
  String dni;
  String direccion;
  String descripcion;
  String telefono;
  String celular;
  String urlFoto;
  double puntuacion;

  Experto({this.id, this.nombre, this.apellidos, this.especialidad,this.etiqueta, this.dni,
      this.direccion,this.descripcion, this.telefono, this.celular, this.urlFoto, this.puntuacion});


}