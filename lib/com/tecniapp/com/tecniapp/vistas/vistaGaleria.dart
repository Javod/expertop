
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tecniapp_v2/com/tecniapp/beans/experto.dart';
import 'package:tecniapp_v2/com/tecniapp/com/tecniapp/vistas/vistaexperto.dart';
import 'package:tecniapp_v2/com/tecniapp/widgetsmodels/modeloItemExperto.dart';

class VistaGaleria extends StatelessWidget
{
   VistaGaleria({Key key, this.titulo}) : super(key: key);

   String titulo;

   final expertos = [Experto(nombre: 'Jose',apellidos: 'Castro Rivera',etiqueta: 'Maestro Constructor', puntuacion: 4.3, descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.'),
     Experto(nombre: 'Camila',apellidos: 'Rodriguez aponte', puntuacion: 3.9,etiqueta: 'Sastre' ,descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.'),
     Experto(nombre: 'Alejandro',apellidos: 'Minaya Rojas', puntuacion: 4.9,etiqueta: 'Experto en melamine' ,descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.'),
     Experto(nombre: 'Xavier',apellidos: 'Pinade López',etiqueta: 'Ebanista', puntuacion: 2.1, descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.'),
     Experto(nombre: 'Jesús Andrés',apellidos: 'Castillo molina',etiqueta: 'Carpintero metalico', puntuacion: 5.0,  descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.'),
     Experto(nombre: 'Saúl',apellidos: 'Luján Dante', puntuacion: 2.9,etiqueta: 'Diseñador gráfico',  descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.'),
     Experto(nombre: 'Rocio',apellidos: 'Cerdán Padilla', puntuacion: 3.2, etiqueta: 'Costurera y diseñadora', descripcion: 'Soy un maestro constructor responsable y competente, que demuesta la calidad en sus trabajos.')
   ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(titulo),
      ),
      body: ListView.builder(
        itemCount: expertos.length,
          itemBuilder: (context,index){
            return GestureDetector(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => VistaExperto(experto: expertos[index],)),
                );
              },
              child: ModeloItemExperto(experto: expertos[index]),
            );
          },
      ),
    );
  }

}