
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tecniapp_v2/com/tecniapp/beans/experto.dart';

class VistaExperto extends StatelessWidget
{
  VistaExperto({Key key, this.experto}): super(key: key);

  Experto experto;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Galeria de : ${experto.nombre}'),
      ),
      body: Card(
        color: Colors.brown,
        child: Container(
          height: 170.0,
          padding: EdgeInsets.all(12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                      width: 90.0,
                      height: 90.0,
                      margin: EdgeInsets.only(right: 8.0),
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: new NetworkImage(
                                  "https://www.blixt.tv/wp-content/uploads/2019/02/fotos-para-cv.-laura-bosh.jpg")
                          )
                      )
                  ),
                  SizedBox(height: 12.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Icon(Icons.thumb_up, color: Colors.white,),
                      SizedBox(width: 12.0,),
                      Text('${experto.puntuacion}', style: TextStyle(color: Colors.white),),
                      SizedBox(width: 8.0,)
                    ],
                  )

                ],
              ),

              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,

                  children: <Widget>[
                    Text('${experto.nombre} ${experto.apellidos}', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                    Text('${experto.etiqueta}', style: TextStyle(color: Colors.white)),
                    SizedBox(height: 12.0,),
                    Text(experto.descripcion, style: TextStyle(color: Colors.white)),
                  ],
                ),
              ),

            ],
          ),
        ),

      ),
    );
  }





}