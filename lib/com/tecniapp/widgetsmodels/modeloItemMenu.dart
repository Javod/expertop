import 'package:flutter/material.dart';
import 'package:tecniapp_v2/main.dart';

class ModeloItemMenu  extends StatelessWidget
{
  ModeloItemMenu({Key key, this.titulo, this.imgCategoria});

  String titulo;
  Icon imgCategoria;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child:Container(
        height: 90.0,
        width: ((MediaQuery.of(context).size.width-32)/2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,

          children: <Widget>[
            imgCategoria,
            Text(titulo,
              overflow: TextOverflow.clip,

            )
          ],
        ),
      ),
    );
  }







}